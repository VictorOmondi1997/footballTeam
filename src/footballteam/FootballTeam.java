/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package footballteam;

/**
 *
 * @author user
 */
public class FootballTeam {

    /**
     * @param args the command line arguments
     */
    Member member;
    public footballTeam(Member member){
        this.member = member;
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Member myMember = new Member("Aureel", "light", 10, 1);
        footballTeam myTeam = new footballTeam(myMember);
        System.out.println(myTeam.getName());
        System.out.println(myTeam.getType());
        System.out.println(myTeam.getLevel());
        System.out.println(myTeam.getRank());
    }
    
}
class Member{
    private String name;
    private String type;
    private int level;
    private int rank;
    
    public Member(String name, String type, int level, int rank){
        this.name = name;
        this.type = type;
        this.level = level;
        this.rank = rank;
    }
    
    /* let's define our getter functions here*/
    public String getName(){//what is your name
        return this.name; //my name is...
    }
    
    public String getType(){//what is your type
        return this.type; //my type is...
    }  
    public int getLevel(){//what is your level
        return this.level; //my level is...
    }
    public int getRank(){//what is your rank
        return this.rank; //my rank is...
    }    
}